import matplotlib.pyplot as plt


f = open("eager_add", "r")
content = f.readlines()
print(len(content))
sum = 0
eager_add = []
for i in content:
    eager_add.append(int(i))
for j in eager_add:
    sum = sum + j
m_eag_add = sum / len(content)
print m_eag_add
f.close()

f = open("eager_ff", "r")
content = f.readlines()
sum = 0
eager_ff = []
for i in content:
    eager_ff.append(int(i))
for j in eager_ff:
    sum = sum + j
m_eag_ff = sum / len(content)
print m_eag_ff
f.close()


f = open("eager_cg", "r")
content = f.readlines()
sum = 0
eager_cg = []
for i in content:
    eager_cg.append(int(i))
for j in eager_add:
    sum = sum + j
m_eag_cg = sum / len(content)
print m_eag_cg
f.close()


f = open("eager_goalcount", "r")
content = f.readlines()
sum = 0
eager_goalcount = []
for i in content:
    eager_goalcount.append(int(i))
for j in eager_goalcount:
    sum = sum + j
m_eag_goalcount = sum / len(content)
print m_eag_goalcount
f.close()

names_eager = ['eager_add', 'eager_ff', 'eager_cg', 'eager_golcount']
values_eager = []
values_eager.append(m_eag_add)
values_eager.append(m_eag_ff)
values_eager.append(m_eag_cg)
values_eager.append(m_eag_goalcount)
plt.bar(names_eager, values_eager)
plt.ylabel('Steps\' average')
plt.show()

f = open("astar_add", "r")
content = f.readlines()
sum = 0
astar_add = []
for i in content:
    astar_add.append(int(i))
for j in astar_add:
    sum = sum + j
m_astar_add = sum / len(content)
print m_astar_add
f.close()

f = open("astar_ff", "r")
content = f.readlines()
sum = 0
astar_ff = []
for i in content:
    astar_ff.append(int(i))
for j in astar_ff:
    sum = sum + j
m_astar_ff = sum / len(content)
print m_astar_ff
f.close()

f = open("astar_cg", "r")
content = f.readlines()
sum = 0
astar_cg = []
for i in content:
    astar_cg.append(int(i))
for j in astar_cg:
    sum = sum + j
m_astar_cg = sum / len(content)
print m_astar_cg
f.close()

f = open("astar_goalcount", "r")
content = f.readlines()
sum = 0
astar_goalcount = []
for i in content:
    astar_goalcount.append(int(i))
for j in astar_goalcount:
    sum = sum + j
m_astar_goalcount = sum / len(content)
print m_astar_goalcount
f.close()

# names_astar = ['astar_add', 'astar_ff', 'astar_cg', 'astar_golcount']
# values_astar = []
# values_astar.append(m_astar_add)
# values_astar.append(m_astar_ff)
# values_astar.append(m_astar_cg)
# values_astar.append(m_astar_goalcount)
# plt.bar(names_astar, values_astar)
# plt.ylabel('Steps\' average')
# plt.show()

f = open("lazy_add", "r")
content = f.readlines()
sum = 0
lazy_add = []
for i in content:
    lazy_add.append(int(i))
for j in lazy_add:
    sum = sum + j
m_lazy_add = sum / len(content)
print m_lazy_add
f.close()

f = open("lazy_ff", "r")
content = f.readlines()
sum = 0
lazy_ff = []
for i in content:
    lazy_ff.append(int(i))
for j in lazy_ff:
    sum = sum + j
m_lazy_ff = sum / len(content)
print m_lazy_ff
f.close()


f = open("lazy_cg", "r")
content = f.readlines()
sum = 0
lazy_cg = []
for i in content:
    lazy_cg.append(int(i))
for j in lazy_cg:
    sum = sum + j
m_lazy_cg = sum / len(content)
print m_lazy_cg
f.close()


f = open("lazy_goalcount", "r")
content = f.readlines()
sum = 0
lazy_goalcount = []
for i in content:
    lazy_goalcount.append(int(i))
for j in lazy_goalcount:
    sum = sum + j
m_lazy_goalcount = sum / len(content)
print m_lazy_goalcount
f.close()
#
# names_lazy = ['lazy_add', 'lazy_ff', 'lazy_cg', 'lazy_golcount']
# values_lazy = []
# values_lazy.append(m_lazy_add)
# values_lazy.append(m_lazy_ff)
# values_lazy.append(m_lazy_cg)
# values_lazy.append(m_lazy_goalcount)
# plt.bar(names_lazy, values_lazy)
# plt.ylabel('Steps\' average')
# plt.show()

# names_final = ['eager_ff', 'lazy_ff', 'astar_golcount']
# values_final = []
# values_final.append(m_eag_ff)
# values_final.append(m_lazy_cg)
# values_final.append(m_astar_goalcount)
# plt.bar(names_final, values_final)
# plt.ylabel('Steps\' average')
# plt.show()
