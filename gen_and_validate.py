''' Structural Game for 15 - Puzzle with different difficulty levels'''
from random import randint
import os

class Puzzle:
    def __init__(self):
        self.items = {}
        self.position = None

    def format(self, ch):
        ch = ch.strip()
        if len(ch) == 1:
            return '  ' + ch + '  '
        elif len(ch) == 2:
            return '  ' + ch + ' '
        elif len(ch) == 0:
            return '     '

    def change(self, to):
        fro = self.position
        for a, b in self.items.items():
            if b == str(to):
                to = a
                break
        self.items[fro], self.items[to] = self.items[to], self.items[fro]
        self.position = to

    def build_board(self, difficulty):
        tiles = []
        final_tiles = []
        for i in range(1, 17):
            self.items[i] = self.format(str(i))
        tmp = 0
        for a, b in self.items.items():
            if b == '  16 ':
                self.items[a] = '     '
                tmp = a
                break
        self.position = tmp
        if difficulty == 0:
            diff = 20
        elif difficulty == 1:
            diff = 50
        else:
            diff = 100
        for _ in range(diff):
            lst = self.valid_moves()
            lst1 = []
            for j in lst:
                lst1.append(int(j.strip()))
            self.change(lst1[randint(0, len(lst1) - 1)])
        for a, b in self.items.items():
            tiles.append(b)
        print type(tiles)
        print tiles
        for i in tiles:
            if i != '     ':
                for s in i.split():
                    if s.isdigit():
                        final_tiles.append(s)
            else:
                final_tiles.append('')
        return final_tiles

    def valid_moves(self):
        pos = self.position
        if pos in [6, 7, 10, 11]:
            return self.items[pos - 4], self.items[pos - 1], \
                   self.items[pos + 1], self.items[pos + 4]
        elif pos in [5, 9]:
            return self.items[pos - 4], self.items[pos + 4], \
                   self.items[pos + 1]
        elif pos in [8, 12]:
            return self.items[pos - 4], self.items[pos + 4], \
                   self.items[pos - 1]
        elif pos in [2, 3]:
            return self.items[pos - 1], self.items[pos + 1], self.items[pos + 4]
        elif pos in [14, 15]:
            return self.items[pos - 1], self.items[pos + 1], \
                   self.items[pos - 4]
        elif pos == 1:
            return self.items[pos + 1], self.items[pos + 4]
        elif pos == 4:
            return self.items[pos - 1], self.items[pos + 4]
        elif pos == 13:
            return self.items[pos + 1], self.items[pos - 4]
        elif pos == 16:
            return self.items[pos - 1], self.items[pos - 4]


def write_goal():
    rows = 4
    cols = 4
    f = open("generated_puzzle.pddl", "a")
    if not f:
        raise ValueError("Output file not opened")
    f.write("\t(:goal\n")
    f.write("\t\t(and\n")
    for row in range(1, rows + 1):
        for col in range(1, cols + 1):
            if (row, col) == (rows, cols):
                continue
            tile = (row - 1) * cols + col
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(col) + ")\n")

    f.write("\t\t)\n")
    f.write("\t)\n")
    f.write(")\n")
    f.close()


def write_init_state(tiles2):
    space_list = ['']
    data2 = tiles2.split('  ')
    if len(data2) == 2:
        tiles = data2[0].split() + space_list + data2[1].split()
    else:
        tiles = space_list + data2[0].split()
    rows = 4
    cols = 4
    f = open("generated_puzzle.pddl", "a")
    if not f:
        raise ValueError("Output file not opened")
    print tiles
    print type(tiles)
    f.write("\t(:init\n")
    for row in range(1, rows):
        f.write("\t\t(next-row row" + str(row) + " row" + str(row + 1) + ")\n")

    for col in range(1, cols):
        f.write("\t\t(next-column col" + str(col) + " col" + str(col + 1) + ")\n")
    column = 1
    for tile in tiles[0:4]:
        row = 1
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[4:8]:
        row = 2
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[8:12]:
        row = 3
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[12:16]:
        row = 4
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    f.write("\t)\n")
    f.close()


def write_final_file(tiles):
    f = open("generated_puzzle.pddl", "w")
    if not f:
        raise ValueError("Output file not created")
    f.write("(define (problem fifteen-puzzle)\n")
    f.write("\t(:domain sliding-puzzle )\n")
    f.write("\t(:objects tile1 tile2 tile3 tile4 tile5 tile6 tile7 tile8 tile9 tile10 tile11 tile12 tile13 tile14 tile15 row1 row2 row3 row4 col1 col2 col3 col4)\n")
    f.close()
    write_init_state(tiles)
    write_goal()


def get_inversions(arr):
    inv_count = 0
    for i in range(len(arr)):
        for j in range(i + 1, len(arr)):
            if (arr[i] > arr[j]):
                inv_count += 1

    return inv_count

def get_corect_states():
    f = open("file.txt", "r")
    states = f.readlines()
    space_list = ['']
    for state in states:
        data2 = state.split('  ')
        if len(data2) == 2:
            tiles = data2[0].split() + space_list + data2[1].split()
        else:
            tiles = space_list + data2[0].split()
        list_1_3 = tiles[12:16] + tiles[4:8]
        list_2_4 = tiles[0:4] + tiles[8:12]
        if (get_inversions(tiles) % 2 == 0 and '' in list_1_3) or (get_inversions(tiles) % 2 ==1 and '' in list_2_4 ):
            f = open("valid_states", "a+")
            f.write(state)
            f.close()
if __name__ == '__main__':
    vect = []
    f = open("file.txt", "r")
    states = f.readlines()
    print len(states)
    for i in range(len(states)):
        write_final_file(states[i])
        os.system("python /home/georgiana/downward/fast-downward.py /home/georgiana/licenta/fifteenPuzzle.pddl /home/georgiana/licenta/generated_puzzle.pddl --search \"eager_greedy([goalcount()], preferred=[goalcount()])\"")
        f = open("sas_plan", "r")
        if not f:
            vect.append(0)
        else:
            content = f.readlines()
            dim = len(content)
            last_line = content[dim - 1]
            nr = int(filter(str.isdigit, last_line))
            vect.append(nr)
            f.close()
    f2 = open("eager_goalcount", "w+")
    for nr in vect:
        f2.write(str(nr) + '\n')
    f2.close()




