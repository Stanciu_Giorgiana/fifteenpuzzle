import cv2
import pytesseract
import os
from gtts import gTTS
from random import randint
from tkinter import *
# - *- coding: utf- 8 - *-

button_flag = 0

def frontend():
    window = Tk()
    window.geometry('300x300')
    l1= Label(window, text="Hello, User!")
    l1.pack()
    l2 = Label(window, text="Welcome to our museum!")
    l2.pack()
    b1 = Button(window, text="Register state", command = register_state)
    if button_flag == 0:
        b1['state'] = 'normal'
    else:
        b1['state'] = 'disabled'
    b1.pack()
    b2= Button(window, text="Next state", command = next_state)
    b2.pack()

    window.mainloop()

def image_processing():
    list = []
    listfinal = []
    grid1 = []

    #p2 nu gaseste sol
    #p3 - left, p4 right, p42 up, p5 down,
    img1 = cv2.imread('p7.jpg')
    # img1 = cv2.imread(read_last_image(', 'jpg'))
    dim = (1050, 716)
    img = cv2.resize(img1, dim, interpolation=cv2.INTER_AREA)
    cv2.imshow("resized", img)
    C, H, W = img.shape[::-1]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, th3 = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
    cv2.imshow("thresh", th3)
    contours, _ = cv2.findContours(th3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for i in range(len(contours)):
        perimeter = cv2.arcLength(contours[i], True)
        if perimeter > 260 and perimeter < 550:
            cv2.drawContours(img, contours, i, (0, 255, 0), 1)
            [x, y, w, h] = cv2.boundingRect(contours[i])
            list.append([x, y, w, h])
    cv2.imshow("original", img)
    print list
    list1 = list[0:4]
    list2 = list[4:8]
    list3 = list[8:12]
    list4 = list[12:16]

    ziped_list1 = zip(*list1)
    ziped_list2 = zip(*list2)
    ziped_list3 = zip(*list3)
    ziped_list4 = zip(*list4)

    x_list1 = ziped_list1[0]
    dic1 = dict(zip(x_list1, list1))
    x_list11 = sorted(x_list1)

    x_list2 = ziped_list2[0]
    dic2 = dict(zip(x_list2, list2))
    x_list21 = sorted(x_list2)

    x_list3 = ziped_list3[0]
    dic3 = dict(zip(x_list3, list3))
    x_list31 = sorted(x_list3)

    x_list4 = ziped_list4[0]
    dic4 = dict(zip(x_list4, list4))
    x_list41 = sorted(x_list4)

    for x in x_list41:
        [x, y, w, h] = dic4[x]
        listfinal.append([x, y, w, h])
    for x in x_list31:
        [x, y, w, h] = dic3[x]
        listfinal.append([x, y, w, h])
    for x in x_list21:
        [x, y, w, h] = dic2[x]
        listfinal.append([x, y, w, h])
    for x in x_list11:
        [x, y, w, h] = dic1[x]
        listfinal.append([x, y, w, h])
    print listfinal

    for x, y, w, h in listfinal:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 255), 3)
        cropped = img[y:y + h - 17, x:x + w - 20]
        cv2.imshow("cropped", cropped)
        dim2 = (100, 100)
        cropped = cv2.resize(cropped, dim2, interpolation=cv2.INTER_AREA)
        gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
        cv2.imshow("cropped", thresh)
        result = pytesseract.image_to_string(thresh, config='-psm 9 -c tessedit_char_whitelist=0123456789')
        print result
        if not result:
            print("am spatiu")
            grid1.append(result)
        else:
            if int(result) > 15:
                print("sunt mai mare")
                result = pytesseract.image_to_string(thresh, config='-psm 7 -c tessedit_char_whitelist=0123456789')
                cv2.imshow("greseala", thresh)
                print result
                grid1.append(result)
            else:
                print("sunt ok")
                grid1.append(result)
    print (grid1)
    return grid1
    cv2.waitKey(0)
    cv2.destroyAllWindows()

tiles = image_processing()

def write_goal():
    rows = 4
    cols = 4
    f = open("generated_puzzle.pddl", "a")
    if not f:
        raise ValueError("Output file not opened")
    f.write("\t(:goal\n")
    f.write("\t\t(and\n")
    for row in range(1, rows + 1):
        for col in range(1, cols + 1):
            if (row, col) == (rows, cols):
                continue
            tile = (row - 1) * cols + col
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(col) + ")\n")

    f.write("\t\t)\n")
    f.write("\t)\n")
    f.write(")\n")
    f.close()


def write_init_state():

    rows = 4
    cols = 4
    f = open("generated_puzzle.pddl", "a")
    if not f:
        raise ValueError("Output file not opened")
    print tiles
    f.write("\t(:init\n")
    for row in range(1, rows):
        f.write("\t\t(next-row row" + str(row) + " row" + str(row + 1) + ")\n")

    for col in range(1, cols):
        f.write("\t\t(next-column col" + str(col) + " col" + str(col + 1) + ")\n")
    column = 1
    for tile in tiles[0:4]:
        row = 1
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[4:8]:
        row = 2
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[8:12]:
        row = 3
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    column = 1
    for tile in tiles[12:16]:
        row = 4
        if not tile:
            f.write("\t\t(is-blank row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
        else:
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(column) + ")\n")
            column = column + 1
    f.write("\t)\n")
    f.close()


def write_final_file():
    f = open("generated_puzzle.pddl", "w")
    if not f:
        raise ValueError("Output file not created")
    f.write("(define (problem fifteen-puzzle)\n")
    f.write("\t(:domain sliding-puzzle )\n")
    f.write("\t(:objects tile1 tile2 tile3 tile4 tile5 tile6 tile7 tile8 tile9 tile10 tile11 tile12 tile13 tile14 tile15 row1 row2 row3 row4 col1 col2 col3 col4)\n")
    f.close()
    write_init_state()
    write_goal()


def read_file_register():
    f = open("plan.txt", "r")
    output_list = f.read()
    speak = gTTS(output_list, lang='en')
    speak.save("plan.mp3")
    os.system("mpg321 plan.mp3")
    f.close()

def edit_file_to_read():
    os.system("cut -d' ' -f1-2 sas_plan > sas_plan2")
    f = open("sas_plan2", "r")
    lines = f.readlines()
    lines = lines[:-1]
    file("sas_plan2", 'w').writelines(lines)
    f.close()
    f = open("sas_plan2", "r")
    plan = open("plan.txt", "w")
    text_read = f.readlines()
    if flag == 1:
        plan.write("Hello! Nice to meet you! In order to solve your game, next step is \n\n")
    else:
        s = verify_move()
        plan.write(s)
    plan.write(text_read[0])
    plan.close()
    f.close()

def random_init_state():
    rows=4
    cols=4
    f = open("generated_puzzle.pddl", "a")
    if not f:
        raise ValueError("Output file not opened")
    f.write("\t(:init\n")
    tiles = list(range(1, rows * cols))
    blank = (randint(1, rows), randint(1, cols))
    blockers = [blank]

    blockers.remove(blank)
    for (grow, gcol) in blockers:
        tile = (grow - 1) * cols + gcol
        f.write("\t\t(tile-at tile" + str(tile) + " row" + str(grow) + " col" + str(gcol) + ")\n")

    blockers.append(blank)
    for row in range(1, rows + 1):
        for col in range(1, cols + 1):
            if (row, col) in blockers:
                continue
            tile = tiles[randint(0, len(tiles) - 1)]
            tiles.remove(tile)
            f.write("\t\t(tile-at tile" + str(tile) + " row" + str(row) + " col" + str(col) + ")\n")

    f.write("\t\t(is-blank row" + str(blank[0]) + " col" + str(blank[1]) + ")\n")
    f.write("\t)\n")

def read_last_image(dirpath, valid_extensions=('jpg','jpeg','png')):
    valid_files = [os.path.join(dirpath, filename) for filename in os.listdir(dirpath)]
    valid_files = [f for f in valid_files if '.' in f and f.rsplit('.', 1)[-1] in valid_extensions and os.path.isfile(f)]

    if not valid_files:
        raise ValueError("No valid images in %s" % dirpath)

    return max(valid_files, key=os.path.getmtime)

def give_text(mode):
    good = []
    wrong = []
    good.append('Great! Now \n\n')
    good.append('That\'s great, next step is \n\n')
    good.append('Perfect! Next step is \n\n')
    good.append('Good job, next step is \n\n')
    good.append('Smart choice! Next step is \n\n')
    good.append('What a great move! Now \n\n')
    wrong.append('That\'s not what I said! Now, I need to make another plan for you! So, next step is\n\n')
    wrong.append('No, no, is not ok what you did! I will make another plan! Now, next step is\n\n')
    wrong.append('Hei, you fooled me! I will give you another plan. Next step is \n\n')
    wrong.append('You cheated! But I prepared another plan. Next step is \n\n')
    wrong.append('Nice try to fool me! I have another plan for you! Now \n\n')
    wrong.append('You can not blocked me! I just prepared another plan for you! Next step is \n\n')

    nr = randint(0, 5)
    if mode == 1:
        return good[nr]
    elif mode == 0:
        return wrong[nr]


def verify_move():
    f= open("last_state", "r")
    content = f.readlines()
    dim = len(content[1])
    move = content[1]
    space_list = [' ']
    data2 = content[0].split('  ')
    data = data2[0].split() + space_list + data2[1].split()
    if move[dim-3].isdigit() and move[dim-2].isdigit():
        number = move[dim-3] + move[dim-2]
    else:
        number = move[dim-2]
    print "number type"
    print type(number)
    directive = move[11]
    nr_index = 0
    nr_space = 0
    if directive == 'd':
        for nr in data:
            if nr == number:
                nr_index = data.index(nr)
            if nr == ' ':
                nr_space= data.index(nr)
        if tiles[nr_index] == u'' and tiles[nr_space].encode('ascii') == number:
            return give_text(1)
        else:
            return give_text(0)
    elif directive == 'l':
        for tile in tiles:
            tile_str = tile.encode('ascii')
            if tile_str == number:
                i = tiles.index(tile)
                if tiles[i + 1] == u'':
                    return give_text(1)
                else:
                    return give_text(0)
    elif directive == 'u':
        for nr in data:
            if nr == number:
                nr_index = data.index(nr)
            if nr == ' ':
                nr_space= data.index(nr)
        if tiles[nr_index] == u'' and tiles[nr_space].encode('ascii') == number:
            return give_text(1)
        else:
            return give_text(0)
    elif directive == 'r':
        for tile in tiles:
            tile_str = tile.encode('ascii')
            if tile_str == number:
                i = tiles.index(tile)
                if tiles[i-1] == u'':
                    return give_text(1)
                else:
                    return give_text(0)

def register_state():
    global flag
    global button_flag
    flag = 1
    image_processing()
    write_final_file()
    os.system("python /home/georgiana/downward/fast-downward.py /home/georgiana/licenta/fifteenPuzzle.pddl /home/georgiana/licenta/generated_puzzle.pddl --search \"astar(goalcount)\" ")
    edit_file_to_read()
    state = open("last_state", "w")
    for x in tiles:
        state.write(x + " ")
    state.write("\n")
    move = open("plan.txt", "r")
    content = move.readlines()
    state.write(content[2])
    move.close()
    state.close()
    read_file_register()
    button_flag = 1

def next_state():
    global flag
    flag = 0
    image_processing()
    write_final_file()
    os.system("python /home/georgiana/downward/fast-downward.py /home/georgiana/licenta/ fifteenPuzzle.pddl /home/georgiana/licenta/generated_puzzle.pddl --search \"astar(goalcount)\" ")
    edit_file_to_read()
    verify_move()
    state = open("last_state", "w")
    for x in tiles:
        state.write(x + " ")
    state.write("\n")
    move = open("plan.txt", "r")
    content = move.readlines()
    state.write(content[2])
    move.close()
    state.close()
    read_file_register()

if __name__ == '__main__':
    frontend()
